import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueResource from 'vue-resource'
import * as VueGoogleMaps from "vue2-google-maps";
import "vue-lazy-youtube-video/dist/style.simplified.css";

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDC1-TBn0PZkzHqRaHtnPCdhzpokU__-LM",
    libraries: "places" 
  }
});

Vue.use(VueResource);
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
